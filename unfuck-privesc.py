#!/usr/bin/env python3

# Rolf Risiko

import platform
import os
import getpass

# Identify OS and release version
platform_os = platform.system()
print("[+] Running on: " + platform_os)
print("[+] Relase:     " + platform.release())

# Infos about current user
print("[+] Running as: " + getpass.getuser())

# Try to access root folder
if platform_os == "Darwin":
    try:
        print("[+] Trying to access roots folder: ")
        print(os.listdir('/var/root/'))
    except:
        print("[-] Could not access roots folder")
else:
    try:
        print("[+] Trying to access roots folder: ")
        print(os.listdir('/root/'))
    except: 
        print("[-] Could not access roots folder")

